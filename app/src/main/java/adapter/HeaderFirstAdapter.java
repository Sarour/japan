package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bd.com.jpan.japantest.R;
import model.Common;
import utils.AppConstant;

public class HeaderFirstAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private List<Common> list = new ArrayList<Common>();
    private Context context;


    public HeaderFirstAdapter(List<Common> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
            return new VHItem(viewItem);
        } else if (viewType == TYPE_HEADER) {
            View viewHead = LayoutInflater.from(parent.getContext()).inflate(R.layout.head_row, parent, false);
            return new VHHeader(viewHead);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHItem) {
            final Common dataItem = getItem(position);

            ((VHItem) holder).textISpending.setEnabled(false);
            ((VHItem) holder).texIDate.setText(dataItem.getP_date());
            ((VHItem) holder).textISupplier.setText(dataItem.getP_suppliers());
            ((VHItem) holder).textIIncome.setText(dataItem.getP_income());
            ((VHItem) holder).textISpending.setText(dataItem.getP_spending());
            ((VHItem) holder).textISubject.setText(dataItem.getP_subject());
            ((VHItem) holder).textIItem.setText(dataItem.getP_item());

             AppConstant.hideKeyboard(context,((VHItem) holder).textISupplier);


        } else if (holder instanceof VHHeader) {
            //cast holder to VHHeader and set data for header.
        }

    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

            return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }
   private Common getItem(int position) {
       // return data[position - 1];
       return list.get(position-1);
    }

    class VHItem extends RecyclerView.ViewHolder {
        protected TextView texIDate,textISupplier,textIIncome,textISubject,textIItem;
        private EditText textISpending;
        protected View mRootView;

        public VHItem(View itemView) {
            super(itemView);
            texIDate = (TextView) itemView.findViewById(R.id.texIDate);
            textISupplier = (TextView) itemView.findViewById(R.id.textISupplier);
            textIIncome = (TextView) itemView.findViewById(R.id.textIIncome);
            textISpending = (EditText) itemView.findViewById(R.id.textISpending);
            textISubject = (TextView) itemView.findViewById(R.id.textISubject);
            textIItem = (TextView) itemView.findViewById(R.id.textIItem);

            mRootView = itemView;
        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
       protected TextView texHDate,textHSupplier,textHIncome,textHSpending,textHSubject,textHItem;
        public VHHeader(View itemView) {
            super(itemView);
            texHDate = (TextView) itemView.findViewById(R.id.texHDate);
            textHSupplier = (TextView) itemView.findViewById(R.id.textHSupplier);
            textHIncome = (TextView) itemView.findViewById(R.id.textHIncome);
            textHSpending = (TextView) itemView.findViewById(R.id.textHSpending);
            textHSubject = (TextView) itemView.findViewById(R.id.textHSubject);
            textHItem = (TextView) itemView.findViewById(R.id.textHItem);
        }
    }
}
