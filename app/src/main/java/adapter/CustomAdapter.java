package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bd.com.jpan.japantest.R;
import model.Common;

;


public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private List<Common> listProduct = new ArrayList<Common>();
    private Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        protected TextView texIDate,textISupplier,textIIncome,textISpending,textISubject,textIItem;
        protected View mRootView;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.texIDate = (TextView) itemView.findViewById(R.id.texIDate);
            this.textISupplier = (TextView) itemView.findViewById(R.id.textISupplier);
            this.textIIncome = (TextView) itemView.findViewById(R.id.textIIncome);
            this.textISpending = (TextView) itemView.findViewById(R.id.textISpending);
            this.textISubject = (TextView) itemView.findViewById(R.id.textISubject);
            this.textIItem = (TextView) itemView.findViewById(R.id.textIItem);
            mRootView = itemView;

        }
    }

    public CustomAdapter(List<Common> listProduct, Context context) {
        this.listProduct = listProduct;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView texIDate = holder.texIDate;
        TextView textISupplier = holder.textISupplier;
        TextView textIIncome = holder.textIIncome;
        TextView textISpending = holder.textISpending;
        TextView textISubject = holder.textISubject;
        TextView textIItem= holder.textIItem;




        final Common common = listProduct.get(listPosition);

        texIDate.setText(common.getP_date());
        textISupplier.setText(common.getP_suppliers());
        textIIncome.setText(common.getP_income());
        textISpending.setText(common.getP_spending());
        textISubject.setText(common.getP_subject());
        textIItem.setText(common.getP_item());

        //iconLabel.setText("\u2302");
        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


    }

    @Override
    public int getItemCount() {
        return listProduct.size();
    }

}
