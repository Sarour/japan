package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bd.com.jpan.japantest.R;
import db.DBHandler;
import model.Common;
import utils.AppConstant;

public class GasAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private List<Common> list = new ArrayList<Common>();
    private Context context;
    private DBHandler dbHandler;


    public GasAdapter(List<Common> list, Context context) {
        this.list = list;
        this.context = context;
        dbHandler = new DBHandler(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View viewItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row, parent, false);
            return new VHItem(viewItem);
        } else if (viewType == TYPE_HEADER) {
            View viewHead = LayoutInflater.from(parent.getContext()).inflate(R.layout.head_row, parent, false);
            return new VHHeader(viewHead);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder,final int position) {
        if (holder instanceof VHItem) {
            final Common dataItem = getItem(position);
            //cast holder to VHItem and set data
            // final HashSet<String> hashSet = new HashSet<String>();
            ((VHItem) holder).textISpending.setEnabled(true);
            ((VHItem) holder).texIDate.setText(dataItem.getP_date());
            ((VHItem) holder).textISupplier.setText(dataItem.getP_suppliers());
            ((VHItem) holder).textIIncome.setText(dataItem.getP_income());
            ((VHItem) holder).textISpending.setText(dataItem.getP_spending());
            ((VHItem) holder).textISubject.setText(dataItem.getP_subject());
            ((VHItem) holder).textIItem.setText(dataItem.getP_item());


            ((VHItem) holder).textISpending.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                    if (actionId == EditorInfo.IME_ACTION_DONE) {

                        List<Common> listNew = new ArrayList<Common>();
                        String date = ((VHItem) holder).texIDate.getText().toString().trim();
                        String supplier = ((VHItem) holder).textISupplier.getText().toString().trim();
                        String income = ((VHItem) holder).textIIncome.getText().toString().trim();
                        String spending = ((VHItem) holder).textISpending.getText().toString().trim();
                        String subject = ((VHItem) holder).textISubject.getText().toString().trim();
                        String item = ((VHItem) holder).textIItem.getText().toString().trim();

                        Common common = new Common();
                        common.setP_date(date);
                        common.setP_suppliers(supplier);
                        common.setP_income(income);
                        common.setP_spending(spending);
                        common.setP_subject(subject);
                        common.setP_item(item);
                        ((VHItem) holder).textISpending.setText("");
                         listNew.add(common);
                        dbHandler.insertAllGas(listNew);
                        insert(common);
                        AppConstant.hideKeyboard(context,((VHItem) holder).textISupplier);

                        return true;
                    } else {
                        return false;
                    }

                }
            });


        } else if (holder instanceof VHHeader) {
            //cast holder to VHHeader and set data for header.
        }

    }

    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

            return TYPE_ITEM;
    }



    private boolean isPositionHeader(int position) {
        return position == 0;
    }
    private Common getItem(int position) {
       // return data[position - 1];
       return list.get(position-1);
    }

    class VHItem extends RecyclerView.ViewHolder {
        protected TextView texIDate,textISupplier,textIIncome,textISubject,textIItem;
        private EditText textISpending;
        //protected View mRootView;

        public VHItem(View itemView) {
            super(itemView);
            texIDate = (TextView) itemView.findViewById(R.id.texIDate);
            textISupplier = (TextView) itemView.findViewById(R.id.textISupplier);
            textIIncome = (TextView) itemView.findViewById(R.id.textIIncome);
            textISpending = (EditText) itemView.findViewById(R.id.textISpending);
            textISubject = (TextView) itemView.findViewById(R.id.textISubject);
            textIItem = (TextView) itemView.findViewById(R.id.textIItem);

            //mRootView = itemView;

            if( itemView instanceof EditText ) {
                EditText textISpending = (EditText) itemView;
                //Do your stuff
            }

        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
       protected TextView texHDate,textHSupplier,textHIncome,textHSpending,textHSubject,textHItem;
        public VHHeader(View itemView) {
            super(itemView);
            texHDate = (TextView) itemView.findViewById(R.id.texHDate);
            textHSupplier = (TextView) itemView.findViewById(R.id.textHSupplier);
            textHIncome = (TextView) itemView.findViewById(R.id.textHIncome);
            textHSpending = (TextView) itemView.findViewById(R.id.textHSpending);
            textHSubject = (TextView) itemView.findViewById(R.id.textHSubject);
            textHItem = (TextView) itemView.findViewById(R.id.textHItem);
        }
    }


    public void insert(Common data) {
       // list.add(position, data);
        list.add(data);
       // notifyItemInserted(position);
        notifyDataSetChanged();
    }

}
