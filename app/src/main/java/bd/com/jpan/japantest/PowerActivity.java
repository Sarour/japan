package bd.com.jpan.japantest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import adapter.PowerAdapter;
import db.DBHandler;

public class PowerActivity extends Activity {
    private Context context;
    private Activity activity;
    private RecyclerView power_recycler;
    private PowerAdapter powerAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private DBHandler dbHandler;
    private LinearLayout ivBackPower;
    private RelativeLayout goneViewPower;
    private ImageView closeBtnPower;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_power);
        context = this;
        activity = this;
        dbHandler = new DBHandler(context);

        intUi();
    }

    private void intUi() {
        ivBackPower= (LinearLayout) findViewById(R.id.ivBackPower);
         goneViewPower = (RelativeLayout) findViewById(R.id.goneViewPower);
        closeBtnPower = (ImageView) findViewById(R.id.closeBtnPower);
        power_recycler = (RecyclerView) findViewById(R.id.power_recycler);
        power_recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        power_recycler.setLayoutManager(layoutManager);
        power_recycler.setItemAnimator(new DefaultItemAnimator());

        ivBackPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        closeBtnPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goneViewPower.setVisibility(View.GONE);
            }
        });

   /*     completeBtnPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"click",Toast.LENGTH_SHORT).show();
            }
        });*/
    /*    Log.e("AllPower Size: "+dbHandler.getAllPower().size(),">>>>>>");

        for(int i = 0;i<dbHandler.getAllPower().size();i++){
            Log.e("Date: "+dbHandler.getAllPower().get(i).getP_date(),">>>");
            Log.e("Suppliers: "+dbHandler.getAllPower().get(i).getP_suppliers(),">>>");
            Log.e("Income: "+dbHandler.getAllPower().get(i).getP_income(),">>>");
            Log.e("subject: "+dbHandler.getAllPower().get(i).getP_subject(),">>>");
            Log.e("Item: "+dbHandler.getAllPower().get(i).getP_item(),">>>");


        }*/
        powerAdapter = new PowerAdapter(dbHandler.getAllPower(),context);
        //customAdapter = new CustomAdapter(dbHandler.getAllPower(),context);
        power_recycler.setAdapter(powerAdapter);
    }
}
