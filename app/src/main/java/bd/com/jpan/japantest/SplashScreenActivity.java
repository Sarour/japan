package bd.com.jpan.japantest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;

import db.DBHandler;
import holder.AllGItem;
import holder.AllPItem;


public class SplashScreenActivity extends Activity {
    private Context context;
    private DBHandler dbHandler;


    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }


    Thread splashTread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        context = this;
        dbHandler = new DBHandler(context);
        StartAnimations();
    }

    private void StartAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        TextView textView = (TextView) findViewById(R.id.splash);
        textView.clearAnimation();
        textView.startAnimation(anim);

        splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    // Splash screen pause time
                    while (waited < 4000) {
                        sleep(100);
                        waited += 100;

                    }

                    SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    if (pref.getString("first_time","").equalsIgnoreCase("first")){

                    }else{
                        loadData();
                        editor.putString("first_time", "first");
                        editor.commit();
                    }


                     Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                     intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                     startActivity(intent);
                     SplashScreenActivity.this.finish();


                } catch (InterruptedException e) {
                    // do nothing
                    e.printStackTrace();
                } finally {
                    SplashScreenActivity.this.finish();
                }
            }
        };
        splashTread.start();
    }

    private void  loadData(){
       final Thread threadP =new Thread(new Runnable() {
            @Override
            public void run() {
                powerLoad();
            }
        });

        final Thread threadG =new Thread(new Runnable() {
            @Override
            public void run() {
                gasLoad();
            }
        });

        threadP.start();
        threadG.start();




    }


    private synchronized void powerLoad(){
        String jsonPower= loadJSONFromAssetPower();
        AllPItem allPItem =  parsePower(jsonPower);
        dbHandler.insertAllPower(allPItem.getPower_products());


    /*    for(int i = 0;i<allPItem.getPower_products().size();i++){
            Log.e("Date: "+allPItem.getPower_products().get(i).getP_date(),">>>");
            Log.e("Suppliers: "+allPItem.getPower_products().get(i).getP_suppliers(),">>>");
            Log.e("Income: "+allPItem.getPower_products().get(i).getP_income(),">>>");
            Log.e("subject: "+allPItem.getPower_products().get(i).getP_subject(),">>>");
            Log.e("Item: "+allPItem.getPower_products().get(i).getP_item(),">>>");


        }*/

        Log.e("Power json: "+jsonPower.toString(),">>>>>>>>>");

        //MyTaskPower myTaskPower = new MyTaskPower();
        //myTaskPower.execute(jsonPower);
    }


    private synchronized void gasLoad(){
        String jsonGas= loadJSONFromAssetGas();
        AllGItem allGItem =  parseGas(jsonGas);
        dbHandler.insertAllGas(allGItem.getGas_products());
        Log.e("Gas json: "+jsonGas.toString(),"<<<<<<");
    }

    public String loadJSONFromAssetPower() {
        String json = null;
        try {
            InputStream is = getAssets().open("poweritem.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public String loadJSONFromAssetGas() {
        String json = null;
        try {
            InputStream is = getAssets().open("gasitem.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }


    private AllPItem parsePower(String content){
        GsonBuilder builder = new GsonBuilder();
        Gson g = builder.setPrettyPrinting().create();
        AllPItem allPItem = g.fromJson(new String(content), AllPItem.class);
        return allPItem;
    }

    private AllGItem parseGas(String content){
        GsonBuilder builder = new GsonBuilder();
        Gson g = builder.setPrettyPrinting().create();
        AllGItem allGItem = g.fromJson(new String(content), AllGItem.class);
        return allGItem;
    }

/*
    private class MyTaskPower extends AsyncTask<String ,String, AllPItem> {
        @Override
        protected void onPreExecute() {


        }

        @Override
        protected AllPItem doInBackground(String... params) {

            AllPItem allPItem=  parsePower(params[0]);

            return allPItem;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //updateDisplay(values[0]);
        }

        @Override
        protected void onPostExecute(AllPItem result) {

            dbHandler.insertAllPower(result.getPower_products());
           // List<Common> list = new ArrayList<Common>();
            //list.addAll(result.getPower_products());



        }
    }*/



}
