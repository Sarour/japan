package bd.com.jpan.japantest;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import adapter.GasAdapter;
import db.DBHandler;

public class GasActivity extends Activity {
    private Context context;
    private Activity activity;
    private RecyclerView gas_recycler;
    private GasAdapter gasAdapter;
    private LinearLayout ivBackGas;
    private RecyclerView.LayoutManager layoutManager;
    private DBHandler dbHandler;
   // private TextView completeBtn;
    private RelativeLayout goneViewGas;
    private ImageView closeBtnGas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gas);
        context = this;
        activity = this;
        dbHandler = new DBHandler(context);

        intUi();
    }

    private void intUi() {
        goneViewGas = (RelativeLayout) findViewById(R.id.goneViewGas);
        ivBackGas= (LinearLayout) findViewById(R.id.ivBackGas);
       // completeBtn = (TextView) findViewById(R.id.completeBtn);
        closeBtnGas = (ImageView) findViewById(R.id.closeBtnGas);

        gas_recycler = (RecyclerView) findViewById(R.id.gas_recycler);
        gas_recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        gas_recycler.setLayoutManager(layoutManager);
        gas_recycler.setItemAnimator(new DefaultItemAnimator());

        ivBackGas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
          /*
        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"Click",Toast.LENGTH_SHORT).show();
            }
        });*/

        closeBtnGas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goneViewGas.setVisibility(View.GONE);
            }
        });

    /*    Log.e("AllPower Size: "+dbHandler.getAllPower().size(),">>>>>>");

        for(int i = 0;i<dbHandler.getAllPower().size();i++){
            Log.e("Date: "+dbHandler.getAllPower().get(i).getP_date(),">>>");
            Log.e("Suppliers: "+dbHandler.getAllPower().get(i).getP_suppliers(),">>>");
            Log.e("Income: "+dbHandler.getAllPower().get(i).getP_income(),">>>");
            Log.e("subject: "+dbHandler.getAllPower().get(i).getP_subject(),">>>");
            Log.e("Item: "+dbHandler.getAllPower().get(i).getP_item(),">>>");


        }*/
        gasAdapter = new GasAdapter(dbHandler.getAllGas(),context);
        //customAdapter = new CustomAdapter(dbHandler.getAllPower(),context);
        gas_recycler.setAdapter(gasAdapter);
    }
}
