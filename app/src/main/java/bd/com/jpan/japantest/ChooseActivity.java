package bd.com.jpan.japantest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ChooseActivity extends Activity {
    private Context context;
    private ImageView ivBackChose;
    private TextView tokyoGasBtn;
    private TextView textPowerBtn;
    private RelativeLayout goneViewChoose;
    private ImageView closeBtnChoose;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salescall);
        context =this;
        intUit();
    }

    private void intUit() {
        goneViewChoose = (RelativeLayout) findViewById(R.id.goneViewChoose);
        textPowerBtn = (TextView) findViewById(R.id.textPowerBtn);
        closeBtnChoose = (ImageView) findViewById(R.id.closeBtnChoose);
        ivBackChose = (ImageView) findViewById(R.id.ivBackChose);
        tokyoGasBtn = (TextView) findViewById(R.id.tokyoGasBtn);
        ivBackChose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        tokyoGasBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,GasActivity.class);
                startActivity(intent);
            }
        });

        textPowerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,PowerActivity.class);
                startActivity(intent);
            }
        });

        closeBtnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goneViewChoose.setVisibility(View.GONE);
            }
        });
    }
}