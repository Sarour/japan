package bd.com.jpan.japantest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import adapter.HeaderFirstAdapter;
import db.DBHandler;

public class MainActivity extends Activity {
    private Context context;
    private Activity activity;
    private RecyclerView all_recycler;
    private HeaderFirstAdapter headerAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private DBHandler dbHandler;
    private RelativeLayout goneView;
    private ImageView closeBtn;
    private TextView showBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        activity = this;
        dbHandler = new DBHandler(context);
        intUi();
    }

    private void intUi() {
        goneView = (RelativeLayout) findViewById(R.id.goneView);
        closeBtn = (ImageView) findViewById(R.id.closeBtn);
        showBtn = (TextView) findViewById(R.id.showBtn);

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goneView.setVisibility(View.GONE);
            }
        });

        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,ChooseActivity.class);
                startActivity(intent);
                finish();
            }
        });

        all_recycler = (RecyclerView) findViewById(R.id.all_recycler);
        all_recycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        all_recycler.setLayoutManager(layoutManager);
        all_recycler.setItemAnimator(new DefaultItemAnimator());
        headerAdapter = new HeaderFirstAdapter(dbHandler.getAllCommon(),context);

        all_recycler.setAdapter(headerAdapter);
    }
}
