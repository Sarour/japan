package model;

/**
 * Created by Salahuddin on 3/23/2017.
 */

public class Common {
    private String p_id;
    private String p_date;
    private String p_suppliers;
    private String p_income;
    private String p_spending;
    private String p_subject;
    private String p_item;

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getP_date() {
        return p_date;
    }

    public void setP_date(String p_date) {
        this.p_date = p_date;
    }

    public String getP_suppliers() {
        return p_suppliers;
    }

    public void setP_suppliers(String p_suppliers) {
        this.p_suppliers = p_suppliers;
    }

    public String getP_income() {
        return p_income;
    }

    public void setP_income(String p_income) {
        this.p_income = p_income;
    }

    public String getP_spending() {
        return p_spending;
    }

    public void setP_spending(String p_spending) {
        this.p_spending = p_spending;
    }

    public String getP_subject() {
        return p_subject;
    }

    public void setP_subject(String p_subject) {
        this.p_subject = p_subject;
    }

    public String getP_item() {
        return p_item;
    }

    public void setP_item(String p_item) {
        this.p_item = p_item;
    }
}
