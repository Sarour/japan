package holder;

import java.util.ArrayList;
import java.util.List;

import model.Common;

/**
 * Created by Salahuddin on 3/23/2017.
 */

public class AllGItem {
    private List<Common> gas_products = new ArrayList<Common>();

    public List<Common> getGas_products() {
        return gas_products;
    }

    public void setGas_products(List<Common> gas_products) {
        this.gas_products = gas_products;
    }
}
