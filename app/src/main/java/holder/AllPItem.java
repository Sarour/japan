package holder;

import java.util.ArrayList;
import java.util.List;

import model.Common;

/**
 * Created by Salahuddin on 3/23/2017.
 */

public class AllPItem {

    private List<Common> power_products = new ArrayList<Common>();

    public List<Common> getPower_products() {
        return power_products;
    }

    public void setPower_products(List<Common> power_products) {
        this.power_products = power_products;
    }

}
