package db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import model.Common;


public class DBHandler extends SQLiteOpenHelper {
    private Context context;
    //DB version
    private static final int DB_VERSION = 1;
    //DB name
    private static final String DB_NAME = "db_supllier";
    //Table name
    private static final String TAB_POWER = "tab_power";
    private static final String TAB_GAS = "tab_gas";
    //TAB_POWER columns name
    private static final String POWER_ID="power_id";
    private static final String POWER_DATE= "power_date";
    private static final String POWER_SUPPLIERS = "power_suppliers";
    private static final String POWER_INCOME = "power_income";
    private static final String POWER_SPENDING = "power_spending";
    private static final String POWER_SUBJECT = "power_subject";
    private static final String POWER_ITEM = "power_item";

    //TAB_GAS columns name
    private static final String GAS_ID="gas_id";
    private static final String GAS_DATE= "gas_date";
    private static final String GAS_SUPPLIERS = "gas_suppliers";
    private static final String GAS_INCOME = "gas_income";
    private static final String GAS_SPENDING = "gas_spending";
    private static final String GAS_SUBJECT = "gas_subject";
    private static final String GAS_ITEM = "gas_item";

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Create table Power
        String CREATE_POWER_TAB= "CREATE TABLE "
                + TAB_POWER
                + "("
                + DBHandler.POWER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DBHandler.POWER_DATE + " TEXT,"
                + DBHandler.POWER_SUPPLIERS + " TEXT,"
                + DBHandler.POWER_INCOME + " TEXT,"
                + DBHandler.POWER_SPENDING + " TEXT,"
                + DBHandler.POWER_SUBJECT + " TEXT,"
                + DBHandler.POWER_ITEM + " TEXT " + ")";

        //Create table Gas
        String CREATE_GAS_TAB= "CREATE TABLE "
                + TAB_GAS
                + "("
                + DBHandler.GAS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + DBHandler.GAS_DATE + " TEXT,"
                + DBHandler.GAS_SUPPLIERS + " TEXT,"
                + DBHandler.GAS_INCOME + " TEXT,"
                + DBHandler.GAS_SPENDING + " TEXT,"
                + DBHandler.GAS_SUBJECT + " TEXT,"
                + DBHandler.GAS_ITEM + " TEXT " + ")";

               db.execSQL(CREATE_POWER_TAB);
               db.execSQL(CREATE_GAS_TAB);

    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
         db.execSQL("DROP TABLE IF EXISTS "+TAB_POWER);
         db.execSQL("DROP TABLE IF EXISTS "+TAB_GAS);
        //Create Table again
        onCreate(db);

    }

    public DBHandler(Context context){
        super(context,DB_NAME,null,DB_VERSION);
        this.context =context;
    }

   public void insertAllPower(List<Common> powerData){

        final SQLiteDatabase db = this.getWritableDatabase();



        String values = buildQueryValueForPowerAndGasInsert(powerData);
        String query = "INSERT INTO " + DBHandler.TAB_POWER+ "("
                + DBHandler.POWER_DATE + ", "
                + DBHandler.POWER_SUPPLIERS  + ", "
                + DBHandler.POWER_INCOME + ", "
                + DBHandler.POWER_SPENDING + ", "
                + DBHandler.POWER_SUBJECT + ", "
                + DBHandler.POWER_ITEM + ") VALUES " + values;
        Log.e("Power query: "+query.toString(),">>>>");
           db.execSQL(query);
          db.close();

    }


    public void insertAllGas(List<Common> gasData){

        final SQLiteDatabase db = this.getWritableDatabase();

        String values = buildQueryValueForPowerAndGasInsert(gasData);
        String query = "INSERT INTO " + DBHandler.TAB_GAS+ "("
                + DBHandler.GAS_DATE + ", "
                + DBHandler.GAS_SUPPLIERS  + ", "
                + DBHandler.GAS_INCOME + ", "
                + DBHandler.GAS_SPENDING + ", "
                + DBHandler.GAS_SUBJECT + ", "
                + DBHandler.GAS_ITEM + ") VALUES " + values;

        Log.e("Gas query: "+query.toString(),">>>>");

        db.execSQL(query);
        db.close();

    }


    private String buildQueryValueForPowerAndGasInsert(List<Common> nData){
        //String array of size equals to list size
        String[] valueArray = new String[nData.size()];

        for(int i = 0; i < nData.size(); i++){
            // Fetching model from list one by one
            Common data = nData.get(i);
            String date = data.getP_date();
            String suppliers = data.getP_suppliers();
            String income = data.getP_income();
            String spending = data.getP_spending();
            String subject = data.getP_subject();
            String item = data.getP_item();

            String[] values = new String[]{date, suppliers, income, spending, subject, item};
            // Pass the string array in buildQueryInsertValue
            valueArray[i] = buildQueryInsertValue(values);
        }
        return TextUtils.join(",", valueArray);
    }

    private String buildQueryInsertValue(String[] values){
        for(int i = 0; i < values.length; i++){
            if(values[i] == null){
                values[i] = "";
            }
            values[i] = DatabaseUtils.sqlEscapeString(values[i]);
        }
        return "(" + TextUtils.join(",", values) + ")";

    }

    public List<Common> getAllPower() {
        List<Common> powerList = new ArrayList<Common>();
        // Select All Query
        String selectAllQuery = "SELECT * FROM " + DBHandler.TAB_POWER;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectAllQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Common common = new Common();
                common.setP_date(cursor.getString(cursor.getColumnIndex(POWER_DATE)));
                common.setP_suppliers(cursor.getString(cursor.getColumnIndex(POWER_SUPPLIERS)));
                common.setP_income(cursor.getString(cursor.getColumnIndex(POWER_INCOME)));
                common.setP_spending(cursor.getString(cursor.getColumnIndex(POWER_SPENDING)));
                common.setP_subject(cursor.getString(cursor.getColumnIndex(POWER_SUBJECT)));
                common.setP_item(cursor.getString(cursor.getColumnIndex(POWER_ITEM)));
                // Adding person to list
                powerList.add(common);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return powerList;
    }



    public List<Common> getAllGas() {
        List<Common> powerList = new ArrayList<Common>();
        // Select All Query
        String selectAllQuery = "SELECT * FROM " + DBHandler.TAB_GAS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectAllQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Common common = new Common();
                common.setP_date(cursor.getString(cursor.getColumnIndex(GAS_DATE)));
                common.setP_suppliers(cursor.getString(cursor.getColumnIndex(GAS_SUPPLIERS)));
                common.setP_income(cursor.getString(cursor.getColumnIndex(GAS_INCOME)));
                common.setP_spending(cursor.getString(cursor.getColumnIndex(GAS_SPENDING)));
                common.setP_subject(cursor.getString(cursor.getColumnIndex(GAS_SUBJECT)));
                common.setP_item(cursor.getString(cursor.getColumnIndex(GAS_ITEM)));
                // Adding person to list
                powerList.add(common);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return powerList;
    }


    public List<Common> getAllCommon() {
        List<Common> commonList = new ArrayList<Common>();
        //SELECT id_2,field_2,id_1,field_1 FROM tbl_2 LEFT OUTER JOIN tbl_1 ON id_2=id_1
        // UNION
        // SELECT id_2,field_2,id_1,field_1 FROM tbl_1 LEFT OUTER JOIN tbl_2 ON id_1=id_2;

        String commDataQuery = "SELECT * FROM "+DBHandler.TAB_GAS+ " LEFT OUTER JOIN "+DBHandler.TAB_POWER+ " ON "+DBHandler.GAS_ITEM+ " = "+DBHandler.POWER_ITEM+
                " UNION "+" SELECT * FROM "+DBHandler.TAB_POWER+" LEFT OUTER JOIN "+DBHandler.TAB_GAS+" ON "+DBHandler.POWER_ITEM+" = "+DBHandler.GAS_ITEM;

       // String selectAllQuery = "SELECT * FROM " + DBHandler.TAB_POWER;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(commDataQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Common common = new Common();
                common.setP_date(cursor.getString(1));
                common.setP_suppliers(cursor.getString(2));
                common.setP_income(cursor.getString(3));
                common.setP_spending(cursor.getString(4));
                common.setP_subject(cursor.getString(5));
                common.setP_item(cursor.getString(6));
                // Adding person to list
                commonList.add(common);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return commonList;
    }

    public boolean addCommonPower(Common common) {
        boolean flag= false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHandler.POWER_DATE, common.getP_date());
        values.put(DBHandler.POWER_SUPPLIERS, common.getP_suppliers());
        values.put(DBHandler.POWER_INCOME, common.getP_income());
        values.put(DBHandler.POWER_SPENDING, common.getP_spending());
        values.put(DBHandler.POWER_SUBJECT, common.getP_subject());
        values.put(DBHandler.POWER_ITEM, common.getP_item());
        // Inserting Row
        db.insert(TAB_POWER, null, values);
        flag=true;
        db.close(); // Closing database connection
        return flag;
    }


    public boolean addCommonGas(Common common) {
        boolean flag= false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBHandler.GAS_DATE, common.getP_date());
        values.put(DBHandler.GAS_SUPPLIERS, common.getP_suppliers());
        values.put(DBHandler.GAS_INCOME, common.getP_income());
        values.put(DBHandler.GAS_SPENDING, common.getP_spending());
        values.put(DBHandler.GAS_SUBJECT, common.getP_subject());
        values.put(DBHandler.GAS_ITEM, common.getP_item());
        // Inserting Row
        db.insert(TAB_POWER, null, values);
        flag=true;
        db.close(); // Closing database connection
        return flag;
    }


//End of Insert List of Data into TAB_Power



/*
    //Insert List of Data into TAB_NOTIFICATION without duplicate data
    public void insertAllNotification(List<AlertObj> notificationData){
        final SQLiteDatabase db = this.getWritableDatabase();

        for(int i=0;i<notificationData.size();i++){
            final String isSame = " Select " + " * FROM "
                    + DBHandler.TAB_NOTIFICATION + " where "
                    + DBHandler.NOTIFICATION_TIME + "='" + notificationData.get(i).getTime() + "'";

            final Cursor cursor = db.rawQuery(isSame, null);

            if (cursor.getCount()>0){

            }else{
                String values = buildQueryValueForNotificationInsert(notificationData);
                String query = "INSERT INTO " + DBHandler.TAB_NOTIFICATION+ "("
                        + DBHandler.NOTIFICATION_TYPE + ", "
                        + DBHandler.NOTIFICATION_FREQUENCY  + ", "
                        + DBHandler.NOTIFICATION_TEXT + ", "
                        + DBHandler.NOTIFICATION_STATUS + ", "
                        + DBHandler.NOTIFICATION_TIME + ") VALUES " + values;
                db.execSQL(query);
            }
            cursor.close();
        }

        db.close();
    }

    private String buildQueryValueForNotificationInsert(List<AlertObj> nData){
        //String array of size equals to list size
        String[] valueArray = new String[nData.size()];

        for(int i = 0; i < nData.size(); i++){
            // Fetching model from list one by one
            AlertObj data = nData.get(i);

            String type = data.getAlertType();
            String frequency = data.getFrequency();
            String text = data.getNotificationText();
            String status = data.getStatus();
            String time = ""+ data.getTime();

            String[] values = new String[]{type, frequency, text, status, time};
            // Pass the string array in buildQueryInsertValue
            valueArray[i] = buildQueryInsertValue(values);
        }
        return TextUtils.join(",", valueArray);
    }

    private String buildQueryInsertValue(String[] values){
        for(int i = 0; i < values.length; i++){
            if(values[i] == null){
                values[i] = "";
            }
            values[i] = DatabaseUtils.sqlEscapeString(values[i]);
        }
        return "(" + TextUtils.join(",", values) + ")";


    }
//End of Insert List of Data into TAB_FLOWER


    //All message read from db.
      public List<AlertObj> getAllMessage() {
        List<AlertObj> notificationList = new ArrayList<AlertObj>();
        // Select All Query
        String selectAllQuery = "SELECT * FROM " + DBHandler.TAB_NOTIFICATION;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectAllQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AlertObj notification = new AlertObj();
                notification.setAlertType(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TYPE)));
                notification.setFrequency(cursor.getString(cursor.getColumnIndex(NOTIFICATION_FREQUENCY)));
                notification.setNotificationText(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TEXT)));
                notification.setStatus(cursor.getString(cursor.getColumnIndex(NOTIFICATION_STATUS)));
                notification.setTime(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TIME)));
                // Adding person to list
                notificationList.add(notification);
            } while (cursor.moveToNext());
        }
          cursor.close();
          db.close();
         return notificationList;
    }

    //Is time include in table?
     public boolean isTime(String time) {
        boolean flag = false;
        final SQLiteDatabase db = this.getWritableDatabase();

        final String queryIs = "Select " + "* FROM "
                + DBHandler.TAB_NOTIFICATION + " where "
                + DBHandler.NOTIFICATION_TIME + "='"+time+"'";
         //+" AND "+DBHandler.NOTIFICATION_STATUS+"="+0+"";

         Log.e("check data query: "+queryIs,">>");

        final Cursor cursor = db.rawQuery(queryIs, null);

        if (cursor.getCount()>0) {
            flag = true;
        }else{
            flag = false;
        }
        cursor.close();
        db.close();
        return flag;
    }

    //Delete single message
        public boolean removeSingleMessage(String time) {
        boolean isSuccess = false;
        SQLiteDatabase db = this.getWritableDatabase();
        isSuccess = db.delete(DBHandler.TAB_NOTIFICATION, DBHandler.NOTIFICATION_TIME + "='" + time + "'", null) > 0;
        db.close();
        return isSuccess;
    }
    //Delete all message
       public void deleteAllMessage(){
        final SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ DBHandler.TAB_NOTIFICATION);
        db.close();
    }

     public boolean updateMessage(String time) {
        boolean flag = false;
        final SQLiteDatabase db = getWritableDatabase();

        final String queryStr = "Select " + "* FROM "
                + DBHandler.TAB_NOTIFICATION + " where "
                + DBHandler.NOTIFICATION_TIME + "='" + time + "'";

        final Cursor cursor = db.rawQuery(queryStr, null);

        if (cursor.getCount() > 0) {
            final String queryStrUpdate= "Update "
                    + DBHandler.TAB_NOTIFICATION + " set "
                    + DBHandler.NOTIFICATION_STATUS + "='" + 1 + "'"
                    + " where " + DBHandler.NOTIFICATION_TIME + "='" + time + "'";
            Log.e("update ", ">>" + queryStrUpdate);
            db.execSQL(queryStrUpdate);
            flag = true;
        }
         cursor.close();
        db.close();
        return flag;
    }


    public AlertObj getSingleMessage(String time) {
        // Select Object Query
        final String query = "Select " + "* FROM "
                + DBHandler.TAB_NOTIFICATION + " where "
                + DBHandler.NOTIFICATION_TIME + "='"+time+"'";
        //+" and " + DBHandler.NOTIFICATION_STATUS + "='"+0+"'";
        Log.e("single Obj query"+query,">>>");

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        AlertObj notification = new AlertObj();
        if (cursor.moveToFirst()) {
            do {

                notification.setAlertType(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TYPE)));
                notification.setFrequency(cursor.getString(cursor.getColumnIndex(NOTIFICATION_FREQUENCY)));
                notification.setNotificationText(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TEXT)));
                notification.setStatus(cursor.getString(cursor.getColumnIndex(NOTIFICATION_STATUS)));
                notification.setTime(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TIME)));

            } while (cursor.moveToNext());
        }


        cursor.close();
        db.close();
        return notification;
    }



    public long getTotalItemNotify() {
        long totalNumber = 0;
        final SQLiteDatabase db = getWritableDatabase();

        final String totalCountQuery = "SELECT "
                + "SUM("+DBHandler.NOTIFICATION_STATUS+") as TotalCount FROM "+DBHandler.TAB_NOTIFICATION
                +" where "+DBHandler.NOTIFICATION_STATUS+"='"+1+"'";
        Log.e("notify item: "+totalCountQuery,">>");

         Cursor cursor = db.rawQuery(totalCountQuery, null);

        if (cursor.moveToFirst()) {

            totalNumber = cursor.getInt(cursor.getColumnIndex("TotalCount"));

        }

        Log.e("TotalCount>>>", totalNumber + "");
        cursor.close();
        db.close();

        return totalNumber;
    }


   public long getTotalRows() {
        long totalNumber = 0;
        final SQLiteDatabase db = getWritableDatabase();
        totalNumber = DatabaseUtils.longForQuery(db, " SELECT " + "COUNT(" + "*" + ")" + " FROM " + DBHandler.TAB_NOTIFICATION, null);
        db.close();
        return totalNumber;
    }

    //All message read from db.
    public List<AlertObj> getAllAfterNotification() {
        List<AlertObj> notificationList = new ArrayList<AlertObj>();
        // Select All Query
        String selectAllNotificationMgs = "SELECT * FROM " + DBHandler.TAB_NOTIFICATION
                +" where "+DBHandler.NOTIFICATION_STATUS+"='"+1+"'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectAllNotificationMgs, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AlertObj notification = new AlertObj();
                notification.setAlertType(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TYPE)));
                notification.setFrequency(cursor.getString(cursor.getColumnIndex(NOTIFICATION_FREQUENCY)));
                notification.setNotificationText(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TEXT)));
                notification.setStatus(cursor.getString(cursor.getColumnIndex(NOTIFICATION_STATUS)));
                notification.setTime(cursor.getString(cursor.getColumnIndex(NOTIFICATION_TIME)));
                // Adding person to list
                notificationList.add(notification);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return notificationList;
    }*/

}